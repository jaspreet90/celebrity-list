package com.jaspreet.CelebrityList.ui.celeb.frg

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jaspreet.CelebrityList.R
import com.jaspreet.CelebrityList.listener.FavouriteClick
import com.jaspreet.CelebrityList.repo.db.CelebrityTable
import com.jaspreet.CelebrityList.ui.base.BaseFragment
import com.jaspreet.CelebrityList.ui.celeb.CelebListViewModel
import com.jaspreet.CelebrityList.ui.celeb.adapter.CelebrityAdapter
import kotlinx.android.synthetic.main.frg_celebrity.*
import javax.inject.Inject



open class CeleberityFrg: BaseFragment<CelebListViewModel>() ,FavouriteClick {


    companion object{

       const val TAB_POS="index"

         fun newIntance(index:Int) : CeleberityFrg{

           val bundle = Bundle()
           bundle.putInt(TAB_POS,index)
           val celeberityFrg = CeleberityFrg()
           celeberityFrg.setArguments(bundle)
            return celeberityFrg
       }
    }

    private var mAdapter: CelebrityAdapter? = null
    @Inject
    lateinit var celebListViewModel: CelebListViewModel

    override fun getViewModel(): CelebListViewModel = celebListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.frg_celebrity,container,false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerView()
        celebListViewModel.getCelebrityList()
        celebListViewModel.getFavouriteCelebrityList()
        var index= arguments?.getInt(TAB_POS)
        bindViewModel(index)

    }


    fun bindViewModel(index:Int?){

        if (index==0) {
             bindAllCelebrity()
        } else {
             bindFavouriteCelebrity()
        }
    }

    fun bindAllCelebrity(){
        mAdapter?.tabPosition=0
        celebListViewModel.mutableCelebrityLiveData.observe(this, Observer {
            mAdapter?.celebrityList = it ?: emptyList()
        })


    }

    fun bindFavouriteCelebrity(){
        mAdapter?.tabPosition=1
        celebListViewModel.mutableCelebrityFavouriteLiveData.observe(this, Observer {
            mAdapter?.celebrityList = it ?: emptyList()
        })

    }

    fun setUpRecyclerView(){
        var linearLayoutManager= LinearLayoutManager(requireContext())
        rv_celeb.layoutManager = linearLayoutManager
        mAdapter = CelebrityAdapter(requireContext(), this)
        rv_celeb.adapter = mAdapter
    }


    override fun onFavouriteClick(obj: CelebrityTable) {
        celebListViewModel.updateFavouriteCelebrity( obj)
     }
}

