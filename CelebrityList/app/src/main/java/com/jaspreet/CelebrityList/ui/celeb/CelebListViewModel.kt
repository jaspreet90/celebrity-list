package com.jaspreet.CelebrityList.ui.celeb

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.gson.Gson
import com.jaspreet.CelebrityList.domain.GetCelebValuesUseCase
import com.jaspreet.CelebrityList.repo.db.CelebrityTable
import com.jaspreet.CelebrityList.ui.base.BaseViewModel
import io.reactivex.schedulers.Schedulers


class CelebListViewModel(private val getCelebValuesUseCase: GetCelebValuesUseCase) : BaseViewModel() {


     var mutableCelebrityLiveData =MutableLiveData<List<CelebrityTable>>()

     var mutableCelebrityFavouriteLiveData =MutableLiveData<List<CelebrityTable>>()

     fun getCelebrityList(){
       addDisposable( getCelebValuesUseCase.getCelebrityList()
            .subscribe({

                Log.d("==celeb list=",Gson().toJson(it))
                mutableCelebrityLiveData.postValue(it)

        },{
                Log.d("==celeb error=", it.message)
            })
       )
    }

    fun getFavouriteCelebrityList(){
      addDisposable( getCelebValuesUseCase.getFavouriteCelebrities()
           .subscribeOn(Schedulers.io())
           .observeOn(Schedulers.io())
           .subscribe({
               mutableCelebrityFavouriteLiveData.postValue(it)
           },{

           })
      )
    }

    fun updateFavouriteCelebrity(obj: CelebrityTable){

        obj.isFavourite = !obj.isFavourite!!
        getCelebValuesUseCase.updateFavouriteCelebrity(obj.celebId, obj.isFavourite!!)
    }

}