package com.jaspreet.CelebrityList.repo.db


import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "CELEBRITY_TABLE")
data class CelebrityTable (

    @PrimaryKey
    @ColumnInfo(name="ID")
    var celebId: String,
    @ColumnInfo(name="NAME")
    var name: String? = null,
    @ColumnInfo(name="PRICE")
    var price: String? = null,
    @ColumnInfo(name="IMAGE_URL")
    var imageUrl:String?= null,
    @ColumnInfo(name="IS_FAVOURITE")
    var isFavourite : Boolean?=false
)