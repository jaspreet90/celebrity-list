package com.jaspreet.CelebrityList.ui.celeb

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.CelebrityList.domain.GetCelebValuesUseCase
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class CelebValueViewModelFactory  @Inject constructor(
    private val getCelebValuesUseCase: GetCelebValuesUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CelebListViewModel::class.java)) {

            return CelebListViewModel(getCelebValuesUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}