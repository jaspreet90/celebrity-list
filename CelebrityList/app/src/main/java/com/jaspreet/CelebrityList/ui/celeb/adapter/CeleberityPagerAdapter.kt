package com.jaspreet.CelebrityList.ui.celeb.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.jaspreet.CelebrityList.ui.celeb.frg.CeleberityFrg

class CeleberityPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    // Returns total number of pages
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0
            -> return CeleberityFrg.newIntance(0)
            1
            -> return CeleberityFrg.newIntance(1)

            else -> return null
        }
    }

    // Returns the page name for the top indicator
    override fun getPageTitle(position: Int): CharSequence? {
        return "Page $position"
    }


    companion object {
        private val NUM_ITEMS = 2
    }

}
