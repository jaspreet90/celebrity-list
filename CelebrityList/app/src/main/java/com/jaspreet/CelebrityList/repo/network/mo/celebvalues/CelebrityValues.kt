package com.jaspreet.CelebrityList.repo.network.mo.celebvalues

import com.google.gson.annotations.SerializedName
import com.jaspreet.CelebrityList.util.Constants

data class CelebrityValues (

    @SerializedName("celebId")
    var celebId: String? = null,
    @SerializedName("name")
    var price: String? = null,
    @SerializedName("price")
    var name: String? = null,
    @SerializedName("imageUrl")
    var imageUrl:String?=Constants.imageBaseUrl
)