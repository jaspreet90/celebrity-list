package com.jaspreet.CelebrityList.ui.celeb

import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides

@Module
class CelebModule {

    @Provides
    fun provideViewModel(celebListActivity: CelebListActivity, celebValueViewModelFactory: CelebValueViewModelFactory ) = ViewModelProvider(celebListActivity, celebValueViewModelFactory).get(
        CelebListViewModel::class.java)
}
