package com.jaspreet.CelebrityList.data

import com.jaspreet.CelebrityList.repo.db.CelebrityTable
import com.jaspreet.CelebrityList.repo.network.mo.celebvalues.CelebValueResponse
import java.util.*
import kotlin.collections.ArrayList

object Mapper {

    fun mapFromCelebrityValueToCelebrityDB(it: CelebValueResponse) : List<CelebrityTable> {

        var celebrityListUI= ArrayList<CelebrityTable>()

        for( data in it.celebrityValues?: emptyList()){
            celebrityListUI.add(CelebrityTable(data.celebId?: UUID.randomUUID().toString(),data.price,data.name,data.imageUrl,false))

        }
        return celebrityListUI
    }
}