package com.jaspreet.CelebrityList.ui.celeb.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jaspreet.CelebrityList.R
import com.jaspreet.CelebrityList.listener.FavouriteClick
import com.jaspreet.CelebrityList.repo.db.CelebrityTable
import com.jaspreet.CelebrityList.util.Constants

class CelebrityAdapter (  var context: Context ,private val favouriteClick: FavouriteClick) :
    RecyclerView.Adapter<CelebrityAdapter.ViewHolder>() {

    var celebrityList: List<CelebrityTable> = mutableListOf()
    set(value) {
        field=value
        notifyDataSetChanged()
    }
    var tabPosition=0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_celebrity_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val obj= celebrityList[position]
        holder.name.text =  String.format(context.resources.getString( R.string.celebrity,  obj.name))
        holder.price.text =  String.format( context.resources.getString(R.string.price ,obj.price))

        if(obj.celebId!=null) {
            var imageUrl = Constants.imageBaseUrl + obj.celebId + ".jpg"

            Glide.with(context)
                .load(imageUrl)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView)
        }

        if (obj.isFavourite!!){
            holder.ivFav.setColorFilter(context.resources.getColor(R.color.colorAccent))
        }

        holder.ivFav.setOnClickListener {
            favouriteClick.onFavouriteClick(obj)
        }
    }

    override fun getItemCount(): Int {
        return celebrityList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById<View>(R.id.name) as TextView
        val price: TextView = itemView.findViewById<View>(R.id.price) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.userImage) as ImageView
        val ivFav: ImageView = itemView.findViewById<View>(R.id.iv_fav) as ImageView

    }
}
