package com.jaspreet.CelebrityList.repo.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface CelebrityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(celebrityList: List<CelebrityTable>)

    @Query("SELECT * FROM CELEBRITY_TABLE where IS_FAVOURITE=:isFav ")
    fun getAllCelebrities(isFav: Boolean) :Flowable<List<CelebrityTable>>

    @Query("SELECT * FROM CELEBRITY_TABLE where IS_FAVOURITE=:isFav")
    fun getFavouriteCelebrities(isFav:Boolean) : Flowable<List<CelebrityTable>>

    @Query("UPDATE CELEBRITY_TABLE SET IS_FAVOURITE=:isFav WHERE ID = :id")
    fun setFavourite(id: String, isFav: Boolean)


}