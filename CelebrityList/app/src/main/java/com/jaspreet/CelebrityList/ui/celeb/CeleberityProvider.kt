package com.jaspreet.CelebrityList.ui.celeb

import com.jaspreet.CelebrityList.ui.celeb.frg.CeleberityFrg
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CeleberityProvider {

    @ContributesAndroidInjector(modules = [(CelebModule::class)])
    abstract fun bindFrg(): CeleberityFrg
}