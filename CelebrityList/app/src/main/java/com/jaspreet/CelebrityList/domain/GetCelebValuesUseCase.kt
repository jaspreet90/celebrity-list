package com.jaspreet.CelebrityList.domain

import com.jaspreet.CelebrityList.data.Mapper
import com.jaspreet.CelebrityList.repo.db.CelebrityRepo
import com.jaspreet.CelebrityList.repo.db.CelebrityTable
import com.jaspreet.CelebrityList.repo.network.ApiService
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetCelebValuesUseCase @Inject constructor (private val apiService: ApiService , private val celebrityRepo: CelebrityRepo ){


    fun getCelebrityList() : Flowable<List<CelebrityTable>>{

        getCelebritiesFromApi().subscribe()
        return getCelebritiesFromLocalDB()
    }


    private fun getCelebritiesFromApi()=

        apiService.getCelebrityValues()
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .map {
              return@map  Mapper.mapFromCelebrityValueToCelebrityDB(it)
            }

            .flatMapCompletable {
                 celebrityRepo.insert(it)
            }.subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())


    private fun getCelebritiesFromLocalDB()=
        celebrityRepo.getAllCelebrities(false).observeOn(Schedulers.io()).subscribeOn(Schedulers.io())


     fun getFavouriteCelebrities()=
        celebrityRepo.getAllCelebrities(true).observeOn(Schedulers.io()).subscribeOn(Schedulers.io())



    fun updateFavouriteCelebrity(id:String, isFav:Boolean){

        celebrityRepo.setFavourite(id,isFav)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

}