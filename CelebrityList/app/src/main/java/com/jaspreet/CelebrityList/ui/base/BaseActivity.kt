package com.jaspreet.CelebrityList.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

abstract class BaseActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        if (hasInjector())
            AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

    }


    open fun hasInjector() = true

}