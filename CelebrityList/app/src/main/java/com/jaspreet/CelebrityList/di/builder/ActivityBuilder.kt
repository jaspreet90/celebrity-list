package com.jaspreet.CelebrityList.di.builder

import com.jaspreet.CelebrityList.ui.celeb.CelebListActivity
import com.jaspreet.CelebrityList.ui.celeb.CeleberityProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(CeleberityProvider::class)])
    abstract fun bindCelebFrg(): CelebListActivity

}