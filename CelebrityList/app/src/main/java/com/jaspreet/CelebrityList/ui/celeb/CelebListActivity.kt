package com.jaspreet.CelebrityList.ui.celeb
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import com.jaspreet.CelebrityList.R
import com.jaspreet.CelebrityList.ui.base.BaseActivity
import com.jaspreet.CelebrityList.ui.celeb.adapter.CeleberityPagerAdapter
import javax.inject.Inject
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_celebrity.*


class CelebListActivity : BaseActivity() , HasSupportFragmentInjector {

        @Inject
        lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


        override  fun supportFragmentInjector(): AndroidInjector<Fragment>?  =   fragmentDispatchingAndroidInjector



    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_celebrity)
         setSupportActionBar(tool_bar)
         setUp()
    }

    private fun setUp(){

        tab_layout.addTab(tab_layout.newTab().setText(getString(R.string.all)))
        tab_layout.addTab(tab_layout.newTab().setText(getString(R.string.favourite)))
        tab_layout.tabGravity = TabLayout.GRAVITY_FILL


        var adapterViewPager =   CeleberityPagerAdapter(supportFragmentManager)
        view_pager.adapter = adapterViewPager

        view_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layout))

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {   }

            override fun onTabReselected(tab: TabLayout.Tab) {   }
        })

    }
}