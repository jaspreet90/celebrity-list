package com.jaspreet.CelebrityList.di.component


import com.jaspreet.CelebrityList.CelebrityValueApplication
import com.jaspreet.CelebrityList.di.builder.ActivityBuilder
import com.jaspreet.CelebrityList.di.module.ApplicationModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ApplicationModule::class), (ActivityBuilder::class) ])
interface ApplicationComponent {

    fun inject(app: CelebrityValueApplication)

}