package com.jaspreet.CelebrityList.repo.db

import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class CelebrityRepoImpl  @Inject constructor(private val celebrityDao: CelebrityDao)
    :CelebrityRepo {


    override fun insert(celebrityList: List<CelebrityTable>): Completable = Completable.fromAction {  celebrityDao.insert(celebrityList) }

    override fun getAllCelebrities(isFav: Boolean): Flowable<List<CelebrityTable>>  = celebrityDao.getAllCelebrities(isFav)

    override fun getFavouriteCelebrities(isFav: Boolean): Flowable<List<CelebrityTable>>  = celebrityDao.getFavouriteCelebrities(isFav)


    override fun setFavourite(id: String , isFav:Boolean) : Completable = Completable.fromAction {
        celebrityDao.setFavourite(id, isFav)
    }
}