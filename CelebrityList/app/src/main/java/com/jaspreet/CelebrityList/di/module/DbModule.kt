package com.jaspreet.CelebrityList.di.module

import android.app.Application
import com.jaspreet.CelebrityList.repo.db.AppDatabase
import com.jaspreet.CelebrityList.repo.db.CelebrityRepo
import com.jaspreet.CelebrityList.repo.db.CelebrityRepoImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DbModule {

    @Provides
    @Singleton
    internal fun provideAppDb(application: Application) = AppDatabase.createInstance(application)

    @Provides
    @Singleton
    internal fun provideCelebrityDao(appDb: AppDatabase) = appDb.celebrityDao()


    @Singleton
    @Provides
    internal fun productRepository(celebrityRepoImpl: CelebrityRepoImpl): CelebrityRepo = celebrityRepoImpl


}