package com.jaspreet.CelebrityList.repo.network

 import com.jaspreet.CelebrityList.repo.network.mo.celebvalues.CelebValueResponse
 import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

   @GET("developers/export/JSON")
   fun getCelebrityValues() : Single<CelebValueResponse>

}