package com.jaspreet.CelebrityList.listener

import com.jaspreet.CelebrityList.repo.db.CelebrityTable

interface FavouriteClick {

   fun onFavouriteClick(obj: CelebrityTable)
}