package com.jaspreet.CelebrityList.repo.db


import android.app.Application
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase

@Database(entities = [CelebrityTable::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun celebrityDao(): CelebrityDao

    companion object {

        private val DB_NAME = "repoDatabase.db"


        fun createInstance(application: Application): AppDatabase {
            return Room.databaseBuilder(application, AppDatabase::class.java, DB_NAME)
                .build()
        }

    }
}