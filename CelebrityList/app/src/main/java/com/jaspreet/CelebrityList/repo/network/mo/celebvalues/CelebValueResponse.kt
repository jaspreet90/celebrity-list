package com.jaspreet.CelebrityList.repo.network.mo.celebvalues

import com.google.gson.annotations.SerializedName

data class CelebValueResponse (

    @SerializedName("CelebrityValues")
    var celebrityValues: List<CelebrityValues>? = null

)