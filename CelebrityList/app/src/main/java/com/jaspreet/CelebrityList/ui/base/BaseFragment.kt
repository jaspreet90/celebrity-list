package com.jaspreet.CelebrityList.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment<out VM : BaseViewModel> : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjector()) {
            AndroidSupportInjection.inject(this)
        }

    }


    open fun hasInjector() = true

    abstract fun getViewModel(): VM
}