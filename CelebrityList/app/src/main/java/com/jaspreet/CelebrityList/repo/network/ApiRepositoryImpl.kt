package com.jaspreet.CelebrityList.repo.network

import com.jaspreet.CelebrityList.repo.network.mo.celebvalues.CelebValueResponse
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiRepositoryImpl @Inject
constructor( private val apiService: ApiService ) : ApiRepository {
    override fun getCelebrityValues(): Single<CelebValueResponse>  = apiService.getCelebrityValues()

}
